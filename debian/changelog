libshell-posix-select-perl (0.09-1) unstable; urgency=medium

  * Import upstream version 0.09.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Remove unneeded debian/source/include-binaries.
  * Fix permissions of example README.

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Aug 2023 13:38:42 +0200

libshell-posix-select-perl (0.08-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libshell-posix-select-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 19:16:34 +0100

libshell-posix-select-perl (0.08-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 10:24:01 +0100

libshell-posix-select-perl (0.08-1) unstable; urgency=medium

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  * New upstream release.
  * Add copyright/license for new files.
  * Update years of packaging copyright.
  * Update Upstream-Contact in debian/copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.1.1.
  * Bump debhelper compatibility level to 9.
  * Drop moving around of test reference files which have been updated
    upstream.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 Nov 2017 20:17:26 +0100

libshell-posix-select-perl (0.05-2) unstable; urgency=low

  [ gregor herrmann ]
  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2008/06/msg00039.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Florian Ragwitz
    <rafl@debianforum.de>); Florian Ragwitz <rafl@debianforum.de> moved
    to Uploaders.
  * Add debian/watch.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ryan Niebur ]
  * Remove Florian Ragwitz from Uploaders
  * Close ITA (Closes: #523145)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Add /me to Uploaders.
  * debhelper 7.
  * Set Standards-Version to 3.9.1; remove version from perl build dependency.
  * Convert to source format 3.0 (quilt); now we can use binary reference
    files for the test suite.
  * debian/copyright: DEP5 format.

 -- gregor herrmann <gregoa@debian.org>  Tue, 27 Jul 2010 22:19:09 -0400

libshell-posix-select-perl (0.05-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/rules: Don't fail when perl is smart enough not to create empty
    dirs. (Closes: #467759)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Sat, 05 Apr 2008 17:03:36 +0200

libshell-posix-select-perl (0.05-1.1) unstable; urgency=low

  * Non-maintainer upload (maintainer allowed it)
  * Updated testsuite reference files to cope with changes in Filter::Simple.
    As they contain binary changes, I needed to encode them with uuencode
    (added the sharutils build-dep) and change the build system to extract
    them. (Closes: #353133)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Sun, 23 Apr 2006 19:32:25 +0200

libshell-posix-select-perl (0.05-1) unstable; urgency=low

  * Initial Release (Closes: #324737).

 -- Florian Ragwitz <rafl@debianforum.de>  Tue, 23 Aug 2005 19:56:53 +0200
